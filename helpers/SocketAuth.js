/* eslint-disable no-param-reassign */
const debug = require('debug')('SocketAuth');
const UnauthorizedError = require('../lib/UnauthorizedError');

/**
 * 檢查 options 是否有缺少必要參數
 * @param {string} options SocketAuth 的必填選項
 * @return {boolean}
 */
function checkRequiredOptions(options = {}) {
  const requiredProperties = [
    { key: 'customAuth', type: 'function' },
    { key: 'decodedPropertyName', type: 'string' },
    { key: 'callback', type: 'function' },
  ];
  requiredProperties.forEach((requiredProp) => {
    if (
      !Object.prototype.hasOwnProperty.call(options, requiredProp.key)
      // eslint-disable-next-line valid-typeof
      && typeof options[requiredProp.key] === requiredProp.type
    ) {
      debug(`[checkRequiredOptions] options 必須有 ${requiredProp} 這個屬性`);
      return false;
    }
    return true;
  });
}

/**
 * 驗證 header 物件 的 function
 * @param {object} headers 前端傳來驗證的 header 物件
 */
function validateAuthHeader(headers) {
  let isValid = true;
  const requiredProperties = [
    'token', 'deviceId', 'time', 'checksum',
  ];
  const headerProperties = Object.keys(headers);
  requiredProperties.forEach((requireProperty) => {
    if (headerProperties.indexOf(requireProperty) === -1) {
      isValid = false;
    }
  });
  return isValid;
}

/**
 * options {
 *     failDisconnectTimeout: 驗證失敗時，幾秒後斷線,
 *     timeout: 驗證幾秒內沒成功當成失敗,
 *     decodedPropertyName: 自定義驗證後將資料儲存的 key 值
 *     handshake: 是否使用直接連線設定 header 的方式,
 *     customAuth: 自定義驗證的 token 的 function,
 *     callback: 驗證成功後執行的 function,
 * }
 */
function baiforeAuthorize(userOptions) {
  // 檢查 options 參數
  const isOptionVaild = checkRequiredOptions(userOptions);
  if (isOptionVaild === false) {
    throw new Error('[baiforeAuthorize] options 參數驗證失敗');
  }

  // 是否嚴格檢查 token 是否合法, 預設是
  const options = {
    required: true,
    ...userOptions,
  };

  return async function socketCallback(socket) {
    const server = this.server || socket.server;
    if (!server.$emit) {
      const Namespace = Object.getPrototypeOf(server.sockets).constructor;
      // Add authenticated event if there is no authenticated event in Namespace.events
      if (Namespace.events.indexOf('authenticated') === -1) {
        Namespace.events.push('authenticated');
      }
    }
    let authTimeout;
    if (options.required) {
      authTimeout = setTimeout(() => {
        debug('執行斷線');
        socket.disconnect('unauthorized');
      }, options.timeout || 5000);
    }

    // ################# error handler start #################
    function onSocketAuthError(err, definedCode) {
      if (err) {
        const code = definedCode || 'unknown';
        const error = new UnauthorizedError(code, {
          message: (Object.prototype.toString.call(err) === '[object Object]' && err.message) ? err.message : err,
        });

        options.failDisconnectTimeout = typeof options.failDisconnectTimeout === 'number'
          ? options.failDisconnectTimeout : 0;

        const failIntervalId = setTimeout(() => {
          socket.disconnect('unauthorized');
        }, options.failDisconnectTimeout);

        socket.emit('unauthorized', error, () => {
          debug('驗證失敗');
          if (typeof options.failDisconnectTimeout === 'number') {
            clearTimeout(failIntervalId);
          }
          socket.disconnect('unauthorized');
        });
      }
    }
    // ################## error handler end ##################

    socket.on('authenticate', async (headers) => {
      const isAuthObjValid = validateAuthHeader(headers);
      if (options.required) {
        clearTimeout(authTimeout);
      }
      if (!isAuthObjValid) {
        return onSocketAuthError({
          message: '驗證物件缺少欄位',
        }, 'invalid_token');
      }

      // TODO: 換成自定義驗證函數
      // 必須在此處拿到用戶的相關資訊, 並傳入 callback 中
      const member = await options.customAuth(headers);
      if (!member) {
        return onSocketAuthError({
          message: '無法解出用戶資訊，執行斷線',
        }, 'invalid_token');
      }

      socket[options.decodedPropertyName] = member;

      // 如果有設定驗證成功後的 callback,
      // 則在成功後執行, callback 最後一個參數會是 socket 本身
      if (typeof options.callback === 'function') {
        // 如果有傳聊天室 ID, 額外帶入 callback
        // 並將群組聊天室 ID 寫在 socket.roomId 上
        const { roomId, groupChatRoomId } = headers;
        socket[options.roomIdPropertyName] = (roomId && roomId > 0) ? parseInt(roomId, 10) : 0;

        // 相容舊版本，之後都新版的話可以刪除
        if (groupChatRoomId) {
          socket[options.roomIdPropertyName] = parseInt(groupChatRoomId, 10);
        }

        // 如果 callback result = 1 => 成功
        // callbackResult = 0, 一定有失敗
        const callbackResult = await options.callback(socket);
        if (!callbackResult) {
          socket.emit('errorMessage', 'callback過程缺少參數或是有錯誤');
          socket.disconnect('unauthorized');
          return false;
        }
      }

      // 向 client 端發送驗證成功的事件
      socket.emit('authenticated');
      if (server.$emit) {
        server.$emit('authenticated', socket);
      } else {
        const namespace = (
          server.nsps && socket.nsp && server.nsps[socket.nsp.name]
        ) || server.sockets;
        namespace.emit('authenticated', socket);
      }
      return true;
    });
  };
}

module.exports = baiforeAuthorize;
