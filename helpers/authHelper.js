const request = require('request');
const debug = require('debug')('authHelper');
const { AUTH_API, NOTIFY_API, NOTIFY_KEY } = require('../config/auth');

module.exports = {
  /**
   * 拜佛系統的驗證
   * @param {string} authObj 用戶的驗證資訊
   * @example {
   *   "token": "1",
   *   "deviceId": "2",
   *   "time": "3",
   *   "checksum": "4",
   * }
   */
  baiforeAuth(authObj) {
    return new Promise((resolve, reject) => {
      const options = {
        url: AUTH_API,
        timeout: 10000,
        headers: {
          ...authObj,
        },
      };
      debug('request options: %o', options);
      request.post(options, (err, response, body) => {
        if (err) {
          if (err.code === 'ETIMEDOUT' || err.code === 'ESOCKETTIMEDOUT') {
            debug('[baiforeAuth] 逾時');
            reject(err);
            return;
          }
          debug('[baiforeAuth] 串接驗證 API 失敗: %O', err);
          reject(err);
          return;
        }

        if (response.statusCode !== 200) {
          debug('[baiforeAuth] 驗證回應: %s', body);
          reject(new Error('[AuthHelper] status is not 200'));
          return;
        }

        const { status, msg, data } = JSON.parse(body);

        // 處理驗證失敗 如 token 無效
        if (status !== 1) {
          debug('[baiforeAuth] 驗證回應: %s', body);
          reject(new Error(`[AuthHelper] api msg: ${msg}`));
          return;
        }
        const { member } = data;
        resolve({
          _id: member.memberId,
          name: member.memberName,
          avatar: member.photoUrl,
        });
      });
    });
  },

  /**
   * 推播通知給用戶
   * @param {number} senderId 傳送訊的會員ID
   * @param {number} receiverId 接受訊息的會員ID
   */
  notifyHelper(senderId, receiverId) {
    return new Promise((resolve) => {
      const url = NOTIFY_API.replace('{senderId}', senderId);
      const options = {
        url,
        timeout: 2000,
        headers: {
          notifyKey: NOTIFY_KEY,
        },
        body: {
          receiverId,
        },
        json: true,
      };
      debug('request options: %o', options);
      request.post(options, (err) => {
        if (err) {
          if (err.code === 'ETIMEDOUT' || err.code === 'ESOCKETTIMEDOUT') {
            debug('[baiforeAuth] 推播逾時');
            resolve(0);
            return;
          }
          debug('[baiforeAuth] 串接推播 API 失敗: %O', err);
          resolve(0);
          return;
        }

        resolve(1);
      });
    });
  },
};
