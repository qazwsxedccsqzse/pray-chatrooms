const debug = require('debug')('subscribeChant');
const bluebird = require('bluebird');
const redis = require('redis');

const { REDIS_DEFAULT_CONFIG, PUBSUB_REDIS_HOST_CONFIG } = require('../config/redis');

// init redis client
bluebird.promisifyAll(redis);
const redisSubscriber = redis.createClient(PUBSUB_REDIS_HOST_CONFIG);
const redisClient = redis.createClient(PUBSUB_REDIS_HOST_CONFIG);

const CHANT_CHANNEL = 'chant';
const CHANT_EMIT_LOCK = 'chantEmitLock:';
const LOCK_TIMEOUT_SECONDS = 86400;

function subscribeChant(chantRoom, roomName) {
  debug(`[config] ${JSON.stringify(REDIS_DEFAULT_CONFIG)}`);

  redisSubscriber.on('message', async (channel, redisMessage) => {
    debug(`
        [onRedisMessage] channel: ${channel}, message: ${redisMessage}
    `);

    const {
      id,
      member,
      message: originMessage,
      scriptureName,
      counts,
      transferName,
      toMemberName,
    } = JSON.parse(redisMessage);
    const lockName = CHANT_CHANNEL + id;
    // lock by log id, so other thread can process other redis message
    const locked = await redisClient.setnxAsync(lockName, 'locked');
    debug(`[onRedisMessage] lockName: ${lockName} locked: ${locked}`);
    if (locked) {
      debug(`[onRedisMessage] originMessage: ${originMessage}`);
      // expire the lock if the is not removed properly
      redisClient.expire(CHANT_EMIT_LOCK, LOCK_TIMEOUT_SECONDS);
      const emitMessage = {
        id,
        text: originMessage,
        createdAt: Date.now(),
        user: {
          _id: member.memberId,
          name: member.memberName,
          avatar: member.photoUrl,
        },
        scriptureName,
        counts,
        toMemberName,
        transferName,
      };

      chantRoom.to(roomName).emit('message', emitMessage);
    }
  });

  redisSubscriber.subscribe(CHANT_CHANNEL);
  debug(`
    [subscribeChant] success
  `);
}

module.exports = subscribeChant;
