function CreateChatMessage(message = {}, user = {}) {
  return {
    _id: message.id,
    text: message.message,
    createdAt: message.created_at,
    user,
  };
}

module.exports = CreateChatMessage;
