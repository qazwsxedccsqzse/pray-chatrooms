
class ChatRooms {
  /**
   * UserMap 為所有加入的用戶
   * {
   *     userId: "socket.id"
   * }
   */
  constructor() {
    this.UserMap = new Map();
  }

  /**
   * 取得 UserMap
   */
  getUserMap() {
    return this.UserMap;
  }

  /**
   * 取得目前有多少用戶
   */
  getUserCount() {
    return this.UserMap.size;
  }

  /**
   * 回傳用戶的 socket.id
   * @param {string} userId 用戶ID
   */
  getSocketIdByUserId(userId) {
    return this.UserMap.get(userId);
  }

  /**
   * 新增用戶 ID 到 Map 中
   *
   * @param {string} userId 用戶ID
   * @param {string} socketId socket id
   * @return void
   */
  setUserMap(userId, socketId) {
    this.UserMap.set(userId, socketId);
  }

  /**
   * 將用戶移除 UserMap
   * @param {string} userId 用戶ID
   */
  removeUserFromUserMap(userId) {
    return this.UserMap.delete(userId);
  }
}

module.exports = new ChatRooms();
