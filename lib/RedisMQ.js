const RedisSMQ = require('rsmq');
const debug = require('debug')('RedisSMQ');
const { REDIS_DEFAULT_CONFIG } = require('../config/redis-mq');

const { defaultQueueName } = REDIS_DEFAULT_CONFIG;

class RedisMQ {
  constructor(options) {
    // 預設使用 1 個 message queue
    this.newOptions = { ...REDIS_DEFAULT_CONFIG, ...options };
    this.rsmq = new RedisSMQ(this.newOptions);
  }

  /**
   * 初始建立 MessageQueue，必須 new 完之後馬上呼叫
   */
  async initQueue() {
    const queueNames = await this.listAllQueue();
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < this.newOptions.mqCount; i++) {
      const qname = `${defaultQueueName}_${i}`;
      if (queueNames.indexOf(qname) !== -1) {
        // eslint-disable-next-line no-continue
        continue;
      }
      // eslint-disable-next-line no-await-in-loop
      const createQueueResponse = await this.rsmq.createQueueAsync({
        qname,
      });
      debug(`\n產生 MessageQueue[${defaultQueueName}_${i}], 狀態: %s`, createQueueResponse);
    }
  }

  /**
   * 列出目前所有的 Message Queue
   *
   * @return {Array}
   * @example
   * ["qname1", "qname2"]
   */
  async listAllQueue() {
    const queues = await this.rsmq.listQueuesAsync();
    return queues;
  }

  /**
   * 刪除特定的 Queue, 1是成功, 0是無此 queue
   *
   * @param {string} queueName Queue 的名稱
   * @return {number}
   * @example
   * 1
   */
  async deleteQueue(queueName = '') {
    const deleteResult = await this.rsmq.deleteMessageAsync(queueName);
    return deleteResult;
  }

  /**
   * 刪除所有的 Queue
   *
   * @return {Array}
   * @example
   * [1,1]
   */
  async deleteAllQueue() {
    const queueNames = await this.listAllQueue();
    const responses = [];
    queueNames.forEach(async (queueName) => {
      const result = await this.deleteQueue(queueName);
      responses.push(result);
    });
    return responses;
  }

  /**
   *
   * @param {string} qname Queue 的名稱
   * @param {string} message 要傳遞的訊息
   */
  async sendMessage(qname = '', message = '') {
    const messageId = await this.rsmq.sendMessageAsync({
      qname,
      message,
    });
    debug('新增的 message id: %s', messageId);
    return messageId;
  }

  /**
   * 取得特定的 queue 的訊息
   *
   * @param {string}} qname Queue 的名稱
   * @param {Function} callback 監聽 queue 接收訊息的 function
   */
  async receiveMessage(qname = '') {
    return this.rsmq.receiveMessageAsync({ qname });
  }
}

module.exports = (options) => new RedisMQ(options);
