const debug = require('debug')('redisGroupChat');

const client = require('./Cache');

const ROOM_HASH_NAME = 'node:grouproom';
const ROOM_MAX_COUNT = process.env.GROUP_MAX_COUNT || 100;

class GroupChatRoom {
  /**
   * GroupChatRoomMap 為所有群聊的所有聊天室
   * {
   *   roomId: Map {
   *     userId: socketId
   *   }
   * }
   */
  constructor() {
    this.client = client;
  }

  async getRoomMap() {
    const groupKeys = await this.client.keysAsync(`${ROOM_HASH_NAME}:*`);
    const allGroupRooms = await this.client
      .batch(
        groupKeys.map((groupKey) => ['hgetall', groupKey]),
      )
      .execAsync();

    return allGroupRooms;
  }

  async getRoomCount() {
    const groupKeys = await this.client.keys(`${ROOM_HASH_NAME}:*`);
    return groupKeys.length;
  }

  /**
   * Get current group chat room user count
   * @param {integer} $roomId 群聊房間ID
   * @example
   * {
   *   "node:grouproom:1": {
   *     [userId1]: [socketId1],
   *     [userId2]: [socketId2],
   *   }
   * }
   */
  async getGroupChatRoomCount($roomId) {
    const currentCount = await this.client.hlenAsync(`${ROOM_HASH_NAME}:${$roomId}`);
    return currentCount;
  }

  /**
   * 回傳群聊房間
   *
   * @param {string} roomId 群聊房間 ID
   */
  async getRoomById(roomId) {
    const room = await this.client.hgetallAsync(`${ROOM_HASH_NAME}:${roomId}`);
    return room;
  }

  /**
   * 取得房間內所有的用戶 socket id
   *
   * @param {string} roomId 群聊房間 ID
   * @returns {Array}
   */
  async getAllUserSocketIdsByRoomId(roomId) {
    const socketIds = await this.client.hvalsAsync(`${ROOM_HASH_NAME}:${roomId}`);
    return socketIds;
  }

  /**
   * 將用戶新增到房間的 Map
   *
   * @param {string} roomId 房間 ID
   * @param {string} userId 用戶 ID
   * @param {string} userSocketId 用戶 Socket ID
   * @returns {number} 是否成功, 1:成功加入, 0:失敗
   */
  async addUserToRoom(roomId, userId, userSocketId) {
    const roomKey = `${ROOM_HASH_NAME}:${roomId}`;
    const roomUserCount = await this.client.hlenAsync(roomKey);
    debug(`[${roomKey}] 目前的人數: ${roomUserCount}`);
    if (roomUserCount >= parseInt(ROOM_MAX_COUNT, 10)) {
      debug(`${roomKey} 的人數(${roomUserCount})到達上限 (${ROOM_MAX_COUNT})`);
      return 0;
    }
    await this.client.hsetAsync(roomKey, userId, userSocketId);
    return 1;
  }

  /**
   * 將用戶從移除群組聊天室內移除
   *
   * @param {string} roomId 房間 ID
   * @param {string} userId 用戶 ID
   */
  async removeUserFromRoomMap(roomId, userId) {
    if (!roomId || !userId) {
      debug(`
        [removeUserFromRoomMap] roomId and userId are required
        roomId: ${roomId}, userId: ${userId}
      `);
      return 0;
    }

    const deleteCount = await this.client.hdelAsync(`${ROOM_HASH_NAME}:${roomId}`, userId);
    return deleteCount;
  }
}

module.exports = new GroupChatRoom();
