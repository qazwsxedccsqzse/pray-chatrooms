const client = require('./Cache');

const ROOM_HASH_NAME = 'node:chatroom';
const USER_ROOM_MAP_NAME = 'node:room_map';

class ChatRooms {
  constructor() {
    this.client = client;
  }

  /**
   * 取得 UserMap
   */
  async getUserMap() {
    const hashMap = await this.client.hgetallAsync(ROOM_HASH_NAME);
    return hashMap;
  }

  /**
   * 取得目前有多少用戶
   */
  async getUserCount() {
    const count = await this.client.hlenAsync(ROOM_HASH_NAME);
    return count;
  }

  /**
   * 回傳用戶的 socket.id
   * @param {string} userId 用戶ID
   */
  async getSocketIdByUserId(userId) {
    const socketId = await this.client.hgetAsync(ROOM_HASH_NAME, userId);
    return socketId;
  }

  /**
   * 新增用戶 ID 到 Map 中
   *
   * @param {string} userId 用戶ID
   * @param {string} socketId socket id
   * @return void
   */
  async setUserMap(userId, socketId) {
    await this.client.hsetAsync(ROOM_HASH_NAME, userId, socketId);
  }

  /**
   * 設定用戶目前的房間ID
   * @param {string} userId 用戶ID
   * @param {string} roomId 用戶所在的房間ID
   */
  async setUserRoomId(userId, roomId) {
    if (!roomId) {
      return;
    }
    await this.client.hsetAsync(USER_ROOM_MAP_NAME, userId, roomId);
  }

  /**
   * 取得用戶目前所在的房間ID
   * @param {string} userId 用戶ID
   */
  async getUserRoomId(userId) {
    let roomId = await this.client.hgetAsync(USER_ROOM_MAP_NAME, userId);
    if (!roomId) {
      roomId = 0;
    }
    return parseInt(roomId, 10);
  }

  /**
   * 移除用戶目前所在的房間ID
   * @param {string} userId 用戶ID
   */
  async removeUserRoomId(userId) {
    const keyExists = await this.client.hdelAsync(USER_ROOM_MAP_NAME, userId);
    return keyExists;
  }

  /**
   * 將用戶移除 UserMap
   * @param {string} userId 用戶ID
   */
  async removeUserFromUserMap(userId) {
    const keyExists = await this.client.hdelAsync(ROOM_HASH_NAME, userId);
    return keyExists;
  }
}

module.exports = new ChatRooms();
