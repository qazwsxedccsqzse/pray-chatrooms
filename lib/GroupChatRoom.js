const debug = require('debug')('GroupChatRoom');

class GroupChatRoom {
  /**
   * GroupChatRoomMap 為所有群聊的所有聊天室
   * {
   *   roomId: Map {
   *     userId: socketId
   *   }
   * }
   */
  constructor() {
    this.GroupChatRoomMap = new Map();
  }

  getRoomMap() {
    return this.GroupChatRoomMap;
  }

  getRoomCount() {
    return this.GroupChatRoomMap.size;
  }

  /**
   * 回傳群聊房間
   *
   * @param {string} roomId 群聊房間 ID
   */
  getRoomById(roomId) {
    return this.GroupChatRoomMap.get(roomId);
  }

  /**
   * 取得房間內所有的用戶 socket id
   *
   * @param {string} roomId 群聊房間 ID
   * @returns {Array}
   */
  getAllUserSocketIdsByRoomId(roomId) {
    if (!this.GroupChatRoomMap.has(roomId)) {
      return [];
    }
    const room = this.GroupChatRoomMap.get(roomId);
    return [
      ...room.values(),
    ];
  }

  setRoomMap(roomId) {
    if (!this.GroupChatRoomMap.has(roomId)) {
      this.GroupChatRoomMap.set(roomId, new Map());
    }
  }

  /**
   * 將用戶新增到房間的 Map
   *
   * @param {string} roomId 房間 ID
   * @param {string} userId 用戶 ID
   * @param {string} userSocketId 用戶 Socket ID
   */
  addUserToRoom(roomId, userId, userSocketId) {
    if (!this.GroupChatRoomMap.has(roomId)) {
      this.setRoomMap(roomId);
    }
    const roomMap = this.GroupChatRoomMap.get(roomId);
    roomMap.set(userId, userSocketId);
  }

  /**
   * 將用戶從移除群組聊天室內移除
   *
   * @param {string} roomId 房間 ID
   * @param {string} userId 用戶 ID
   */
  removeUserFromRoomMap(roomId, userId) {
    if (!roomId || !userId) {
      debug(`
        [removeUserFromRoomMap] roomId and userId are required
        roomId: ${roomId}, userId: ${userId}
      `);
      return;
    }
    if (!this.GroupChatRoomMap.has(roomId)) {
      return;
    }
    const roomMap = this.GroupChatRoomMap.get(roomId);
    roomMap.delete(userId);
  }
}

module.exports = new GroupChatRoom();
