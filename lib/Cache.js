const bluebird = require('bluebird');
const redis = require('redis');

const { REDIS_DEFAULT_CONFIG } = require('../config/redis');

bluebird.promisifyAll(redis);

class Cache {
  constructor() {
    this.client = redis.createClient(REDIS_DEFAULT_CONFIG);
    return this.client;
  }
}

module.exports = new Cache();
