module.exports = (sequelize, DataTypes) => {
  const ChatMessage = sequelize.define('ChatMessage', {
    member_id: DataTypes.INTEGER,
    room_sort_key: DataTypes.INTEGER,
    message: DataTypes.STRING,
    show: DataTypes.BOOLEAN,
    is_read: DataTypes.BOOLEAN,
  }, {
    underscored: true,
    tableName: 'chat_messages',
  });

  return ChatMessage;
};
