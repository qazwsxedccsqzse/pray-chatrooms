module.exports = (sequelize, DataTypes) => {
  const ChatMessage = sequelize.define('GroupChatRoom', {
    name: DataTypes.STRING,
    photo_url: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    max_count: DataTypes.INTEGER,
    current_count: DataTypes.INTEGER,
  }, {
    underscored: true,
    tableName: 'group_chat_rooms',
  });

  return ChatMessage;
};
