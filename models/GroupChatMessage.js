module.exports = (sequelize, DataTypes) => {
  const GroupChatMessage = sequelize.define('GroupChatMessage', {
    member_id: DataTypes.INTEGER,
    room_id: DataTypes.STRING,
    message: DataTypes.STRING,
    show: DataTypes.BOOLEAN,
  }, {
    underscored: true,
    tableName: 'group_chat_messages',
  });

  return GroupChatMessage;
};
