module.exports = (sequelize, DataTypes) => {
  const PlayerBlockRecord = sequelize.define('PlayerBlockRecord', {
    member_id: DataTypes.BIGINT,
    blocked_member_id: DataTypes.BIGINT,
    status: DataTypes.STRING(8),
    created_at: DataTypes.DATE,
  }, {
    underscored: true,
    tableName: 'player_block_records',
  });
  PlayerBlockRecord.isBlockedBy = async (memberId, blockedMemberId) => {
    const count = await PlayerBlockRecord.count({
      where: {
        member_id: memberId,
        blocked_member_id: blockedMemberId,
      },
    });
    return count > 0;
  };

  return PlayerBlockRecord;
};
