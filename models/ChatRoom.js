module.exports = (sequelize, DataTypes) => {
  const ChatMessage = sequelize.define('ChatRoom', {
    sort_key: DataTypes.INTEGER,
    last_msg_id: DataTypes.INTEGER,
    is_accept: DataTypes.BOOLEAN,
    show: DataTypes.BOOLEAN,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  }, {
    underscored: true,
    tableName: 'chat_rooms',
  });

  return ChatMessage;
};
