module.exports = (sequelize, DataTypes) => {
  const PlayerFriend = sequelize.define('PlayerFriend', {
    member_id: DataTypes.BIGINT,
    friend_id: DataTypes.BIGINT,
    status: DataTypes.STRING(8),
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  }, {
    underscored: true,
    tableName: 'player_friends',
  });

  PlayerFriend.getStatus = (memberId, friendId) => PlayerFriend.findOne({
    where: {
      member_id: memberId,
      friend_id: friendId,
    },
    attributes: ['status'],
    raw: true, // 回傳純資料，不包ORM物件
    nest: false,
  });

  return PlayerFriend;
};
