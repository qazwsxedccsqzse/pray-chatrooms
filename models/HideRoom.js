module.exports = (sequelize, DataTypes) => {
  const HideRoom = sequelize.define('HideRoom', {
    member_id: DataTypes.INTEGER,
    room_sort_key: DataTypes.INTEGER,
  }, {
    underscored: true,
    tableName: 'hide_rooms',
  });

  HideRoom.removeHideRoom = (roomSortKey) => HideRoom.destroy({
    where: {
      room_sort_key: roomSortKey,
    },
  });

  return HideRoom;
};
