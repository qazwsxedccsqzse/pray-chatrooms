/* eslint-disable no-underscore-dangle */

const Koa = require('koa');
const redis = require('socket.io-redis');
const debug = require('debug')('app');
const Entities = require('html-entities').XmlEntities;

const entities = new Entities();
const app = new Koa();
const server = require('http').Server(app.callback());
const chatRooms = require('socket.io')(server, {
  path: '/chatrooms',
  pingTimeout: 6000,
  pingInterval: 2000,
});
const groupRooms = require('socket.io')(server, {
  path: '/groups',
  pingTimeout: 6000,
  pingInterval: 2000,
});
const chantRooms = require('socket.io')(server, {
  path: '/chant',
  pingTimeout: 6000,
  pingInterval: 2000,
});

const baiforeAuthorize = require('./helpers/SocketAuth');
const ChatRoomMaps = require('./lib/RedisChatRoom');
const GroupRoomMaps = require('./lib/RedisGroupChatRoom');
const CreateChatMessage = require('./lib/ChatMessage');
const subscribeChant = require('./lib/SubscribeChant');
const {
  ChatMessage, GroupChatMessage, ChatRoom, GroupChatRoom,
  HideRoom, PlayerBlockRecord,
} = require('./models');
const authSettings = require('./config/socket/chat-config');
const groupAuthSettings = require('./config/socket/group-config');
const chantAuthSettings = require('./config/socket/chant-config');
const { DECODED_PROPERTY_NAME } = require('./config/socket/index');
const { REDIS_DEFAULT_CONFIG } = require('./config/redis');
const { notifyHelper } = require('./helpers/authHelper');

const PORT = process.env.port || 8888;
const CHANT_ROOM_NAME = 'chantRoom';
const socketAdapter = redis(REDIS_DEFAULT_CONFIG);
chatRooms.adapter(socketAdapter);
groupRooms.adapter(socketAdapter);
chantRooms.adapter(socketAdapter);

debug('\n======= running port on %s =======', PORT);
server.listen(PORT);

// error handling
process.on('unhandledRejection', (reason) => {
  debug('Unhandle Promise Rejection: %o', reason.message);
});

chatRooms
  .on('connection', baiforeAuthorize(authSettings))
  .on('authenticated', async (socket) => {
    // 用戶斷線
    socket.on('disconnect', async (reason) => {
      debug('(disconnect) 用戶ID: %s, 原因: %s', socket[DECODED_PROPERTY_NAME]._id, reason);
      await ChatRoomMaps.removeUserFromUserMap(socket[DECODED_PROPERTY_NAME]._id);
      await ChatRoomMaps.removeUserRoomId(socket[DECODED_PROPERTY_NAME]._id);
    });

    // 接收前端傳來的訊息
    socket.on('send-message', async (data) => {
      const { message: orignalMessage, userId, roomId } = data;
      if (!userId || !orignalMessage || !roomId) {
        return;
      }
      const message = entities.encode(orignalMessage);

      // 防止自己傳給自己
      if (parseInt(userId, 10) === socket.member._id) {
        debug('[錯誤] 自己傳給自己');
        return;
      }

      // 字數太長
      if (message.length > 255) {
        debug('[錯誤] 字數太長');
        return;
      }

      const emitMessage = CreateChatMessage({
        id: -1,
        message,
        created_at: Date.now(),
      }, socket.member);

      const anotherUserSocketId = await ChatRoomMaps.getSocketIdByUserId(userId);
      const isRead = anotherUserSocketId ? 1 : 0;

      // TODO: 檢查用戶是否已經有建立聊天室了
      const savedMessage = await ChatMessage.build({
        member_id: socket.member._id,
        room_sort_key: roomId,
        message,
        show: 1,
        is_read: isRead,
      }).save();

      await ChatRoom.update({
        last_msg_id: savedMessage.dataValues.id,
      }, {
        where: {
          sort_key: roomId,
        },
      });

      await HideRoom.removeHideRoom(roomId);

      emitMessage._id = savedMessage.dataValues.id;
      emitMessage.text = entities.decode(emitMessage.text);

      // 傳訊息到自己聊天室
      chatRooms.to(socket.id).emit('message', emitMessage);

      const isBlockedBy = await PlayerBlockRecord.isBlockedBy(userId, socket.member._id);
      debug(`isBlockedBy: ${JSON.stringify(isBlockedBy)}`);
      if (isBlockedBy) {
        debug('被對方封鎖 ===> 不發送通知和傳訊息到對方聊天室');
        return;
      }

      if (!anotherUserSocketId) {
        await notifyHelper(socket.member._id, userId);
        return;
      }

      // anotherUserSocketId 的房間ID不同時, 不要 emit 訊息
      const anotherUserCurrentRoomId = await ChatRoomMaps.getUserRoomId(userId);
      // 如果前端沒有帶 roomId 過來, 預設一樣 emit 過去
      if (anotherUserCurrentRoomId !== 0 && anotherUserCurrentRoomId !== parseInt(roomId, 10)) {
        debug(`目前發送訊息用戶(${socket.roomId})不在同個房間(${anotherUserCurrentRoomId}) ===> 不 emit 事件`);
        return;
      }

      // 傳訊息至對方聊天室
      chatRooms.to(anotherUserSocketId).emit('message', emitMessage);
    });
  });


groupRooms
  .on('connection', baiforeAuthorize(groupAuthSettings))
  .on('authenticated', (socket) => {
    // 用戶斷線
    socket.on('disconnect', async (reason) => {
      debug('(disconnect) 用戶ID: %s, 原因: %s', socket[DECODED_PROPERTY_NAME]._id, reason);
      if (!socket.roomId) {
        debug('[group] 進入群聊時沒有傳入 group id');
        return;
      }

      const deleteCount = await GroupRoomMaps.removeUserFromRoomMap(
        socket.roomId,
        socket[DECODED_PROPERTY_NAME]._id,
      );
      debug(`目前移除 ${deleteCount} 人`);

      const currentCount = await GroupRoomMaps.getGroupChatRoomCount(socket.roomId);
      debug(`目前群聊剩下 ${currentCount} 人`);

      await GroupChatRoom.update({
        current_count: currentCount,
      }, {
        where: {
          id: socket.roomId,
        },
      });
    });

    socket.on('send-message', async (data) => {
      const { message: orignalMessage, roomId } = data;
      if (!orignalMessage || !roomId) {
        socket.emit('errorMessageFormat', 'message, roomId 為必須');
        return;
      }
      const message = entities.encode(orignalMessage);

      const emitMessage = CreateChatMessage({
        id: -1,
        message,
        created_at: Date.now(),
      }, socket.member);

      const savedMessage = await GroupChatMessage.build({
        member_id: socket.member._id,
        room_id: roomId,
        message,
        show: 1,
      }).save();

      emitMessage._id = savedMessage.dataValues.id;
      emitMessage.text = entities.decode(emitMessage.text);

      const allRoomUsers = await GroupRoomMaps.getAllUserSocketIdsByRoomId(roomId);
      allRoomUsers.forEach((userSocketId) => groupRooms.to(userSocketId).emit('message', emitMessage));
    });
  });

chantRooms
  .on('connection', baiforeAuthorize(chantAuthSettings))
  .on('authenticated', (socket) => {
    socket.join(CHANT_ROOM_NAME);
    // 用戶斷線
    socket.on('disconnect', async (reason) => {
      socket.leave(CHANT_ROOM_NAME);
      debug('(disconnect) 用戶ID: %s, 原因: %s', socket[DECODED_PROPERTY_NAME]._id, reason);
    });
  });

subscribeChant(chantRooms, CHANT_ROOM_NAME);
