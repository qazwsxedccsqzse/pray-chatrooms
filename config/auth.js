module.exports = {
  AUTH_API: process.env.auth_api || 'http://127.0.0.1:8002/api/v1/members/authenticate',
  NOTIFY_API: process.env.NOTIFY_API || 'http://127.0.0.1:8002/api/v1/chats/{senderId}/message-notifications',
  NOTIFY_KEY: process.env.NOTIFY_KEY || 'bai_VtiqAe4BOl$for',
};
