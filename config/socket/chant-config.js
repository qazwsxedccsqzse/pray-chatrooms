/* eslint-disable no-underscore-dangle */

const debug = require('debug')('chant');
const { baiforeAuth } = require('../../helpers/authHelper');
const { DECODED_PROPERTY_NAME } = require('./index');

const authSettings = {
  /**
   * 驗證過後要將用戶資訊存在哪個 property 中
   */
  decodedPropertyName: DECODED_PROPERTY_NAME,

  /**
   * 驗證成功後，可以執行自定義的回呼
   * @param {*} socket
   */
  async callback(socket) {
    return 1;
  },

  /**
   * 自定義的驗證方式
   *
   * @param {Object} authObj API 驗證用的 header 參數
   * @example
   * {
   *   token,
   *   deviceId,
   *   time,
   *   checksum,
   * }
   */
  async customAuth(authObj) {
    try {
      const member = await baiforeAuth(authObj);
      debug('[customAuth] member: %O', member);
      return member;
    } catch (e) {
      debug('[customAuth] errorMessage %s', e.message);
      return false;
    }
  },
};

module.exports = authSettings;
