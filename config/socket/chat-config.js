/* eslint-disable no-underscore-dangle */

const debug = require('debug')('chat');
const ChatRoomMaps = require('../../lib/RedisChatRoom');
const { baiforeAuth } = require('../../helpers/authHelper');
const { DECODED_PROPERTY_NAME } = require('./index');

const authSettings = {
  /**
   * 驗證過後要將用戶資訊存在哪個 property 中
   */
  decodedPropertyName: DECODED_PROPERTY_NAME,

  /**
   * 用戶目前所在的私聊房間
   */
  roomIdPropertyName: 'roomId',

  /**
   * 驗證成功後，可以執行自定義的回呼
   * @param {*} socket
   */
  async callback(socket) {
    await ChatRoomMaps.setUserMap(socket.member._id, socket.id);
    await ChatRoomMaps.setUserRoomId(socket.member._id, socket.roomId);
    return 1;
  },

  /**
   * 自定義的驗證方式
   */
  async customAuth(authObj) {
    try {
      const member = await baiforeAuth(authObj);
      debug('[customAuth] member: %O', member);
      return member;
    } catch (e) {
      debug('[customAuth] errorMessage %s', e.message);
      return false;
    }
  },
};

module.exports = authSettings;
