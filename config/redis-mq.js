const REDIS_DEFAULT_CONFIG = {
  host: process.env.REDIS_HOST || '127.0.0.1',
  port: process.env.REDIS_PORT || 6379,
  ns: process.env.REDIS_MQ_NAMESPACE || 'BaiforeMQ',
  db: process.env.REDIS_MQ_DB || 15,
  mqCount: 1,
  defaultQueueName: process.env.REDIS_QUEUE_NAME || 'baifore',
  password: process.env.REDIS_PWD,
};

module.exports = {
  REDIS_DEFAULT_CONFIG,
};
