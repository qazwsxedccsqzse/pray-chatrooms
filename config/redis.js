const REDIS_DEFAULT_CONFIG = {
  host: process.env.REDIS_HOST || '127.0.0.1',
  port: process.env.REDIS_PORT || 6379,
  db: process.env.REDIS_DB || 10,
  password: process.env.REDIS_PWD,
};

const PUBSUB_REDIS_HOST_CONFIG = {
  host: process.env.PUBSUB_REDIS_HOST || 'pubsub-redis',
  port: process.env.PUBSUB_REDIS_PORT || 6379,
  db: process.env.PUBSUB_REDIS_DB || 4,
  password: process.env.PUBSUB_REDIS_PASSWD || '93kEfsATp2M7vu6A',
};

module.exports = {
  REDIS_DEFAULT_CONFIG,
  PUBSUB_REDIS_HOST_CONFIG,
};
